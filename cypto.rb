

##Generates random num 1-9
def returnRandomNum()
	return 1 + rand(9)
end	
##Generates random lower char
def returnRandomLowerString()
	return (97 + rand(25)).chr
end
##Generates random upper char
def returnRandomUpperString()
	(65 + rand(25)).chr
end
##Returns the key; 
def keyGen(lengthOfKey)
	key = ""
	randomChoice = rand(3)

	#Random Uppercase
	if randomChoice == 0
		for i in 0..lengthOfKey-1
			randomString = returnRandomUpperString()
			key = key + randomString.to_s
		end
	end
	#Random Lowercase
	if randomChoice == 1
		for i in 0..lengthOfKey-1
			randomString = returnRandomLowerString()
			key = key + randomString.to_s
		end
	end
	#Random Int
	if randomChoice == 2
		for i in 0..lengthOfKey-1
			randomNum = returnRandomNum()
			key = key + randomNum.to_s
		end
	end
	return key
end

def start()
	
	lengthOfKey = 1
	mine = true
	puts "Welcome to PiMine"
	puts "Enter 0 to end"

	x = 0
	while(x < 1000000)
		key = keyGen(lengthOfKey)

		puts "key:" + key
		myGuess = ""

		tempLengthOfKey = lengthOfKey


##NEVER LEAVES LOOP
##POSSIBLY BECAUSE SLICE ISNT RETURNING STRING TYPE???
##ONCE FOUND, KEY OUTPUTS AS: "" OR; BLANK TYPE
		while(key != "" || key != " " || !key.empty? || key != "\n" || key != null)			
			myGuess = keyGen(tempLengthOfKey)

			puts myGuess
			sleep(1)

			i = 0
			#Verifys to see if index's match in string, if they do remove index from key
			while(i < tempLengthOfKey)
				keyIndex = key[tempLengthOfKey - 1]
				guessIndex = myGuess[tempLengthOfKey - 1]
				if keyIndex == guessIndex
					key = key.slice!(tempLengthOfKey)
					tempLengthOfKey = tempLengthOfKey - 1
				end
				i = i + 1
			end
		end
		lengthOfKey = lengthOfKey + 1
	end
end

=begin
	input = 1
	threadA = Thread.fork do

		while(input != 0)
			puts input
			input = gets.chomp
		end
		mine = false
	end
	threadB = Thread.fork do
		while(mine == true)
		end
	end


	threadA.join
	threadB.join
=end
start()